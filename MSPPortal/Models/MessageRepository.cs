﻿using MSPPortal.Hubs;
using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MSPPortal.Models
{
    public class MessageRepository
    {
        readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();


        public IEnumerable<NotificationViewModel> GetNotifications()
        {
            var messages = new List<NotificationViewModel>();
            using (var connection = new SqlConnection(_connString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"SELECT Id,Message FROM [dbo].[Notifications] where InStatus='PickedUp' OR InStatus='NearBy' OR InStatus='InPremises'", connection))
                {
                    command.Notification = null;

                    var dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        messages.Add(item: new NotificationViewModel { Id = (int)reader["Id"], Message = (string)reader["Message"] });
                    }
                }

            }
            return messages;


        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {

            if (e.Type == SqlNotificationType.Change)
            {
                var notificationList = _context.Notifications.Where(x => x.IsNotified == false && (x.InStatus == "PickedUp" || x.InStatus == "NearBy" || x.InStatus=="InPremises")).ToList();
                var notificationViewModelList = new List<NotificationViewModel>();
                foreach (var notification in notificationList)
                {
                    var notif = new NotificationViewModel
                    {
                        Id = notification.Id,
                        Message = notification.Message,
                        Grade = notification.GradeId.ToString(),
                        Section = notification.SectionId.ToString(),
                        IsNotified = notification.IsNotified.Value,
                        StudentId = notification.StudentId.ToString()
                    };

                    if (notification.IsNotified == false)
                    {
                        notification.IsNotified = true;
                        _context.SaveChanges();

                    }
                    notificationViewModelList.Add(notif);
                }
                // if (notificationViewModelList.Count > 0)
                MessagesHub.SendMessages(notificationViewModelList);


            }
        }
    }
}
