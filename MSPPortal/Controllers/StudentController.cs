﻿
using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MSPPortal.Controllers
{
    [Authorize]
    public class StudentController : Controller
    {

        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();
        // GET: Student
        [Authorize(Roles = "Admin")]
        //[Route("students")]
        [Route("students/{*catchall}")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetStudentsList()
        {

            var lstPost = new List<AddStudent>();
            try
            {

                lstPost = (from s in _context.Students
                           join gr in _context.Grades on s.Grade equals gr.Id
                           join se in _context.Sections on s.SectionId equals se.SectionId
                           where s.IsActive == true
                           select new AddStudent
                           {
                               Id = s.Id,
                               FirstName = s.FirstName,
                               LastName = s.LastName,
                               SectionName = se.SectionName,
                               GradeName = gr.ClassName,
                               SectionId = s.SectionId,
                               GradeId = s.Grade,
                               DOB = s.DOB,
                               // DOBString = s.DOB.HasValue ? s.DOB.Value.ToShortDateString() : "",
                               MiddleName = s.MiddleName,
                               IsActive = s.IsActive
                           }).ToList();

                return Json(lstPost);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetStudentsListByGradeSection(int GradeId, int SectionId)
        {

            var lstPost = new List<AddStudent>();
            try
            {

                lstPost = (from s in _context.Students
                           join gr in _context.Grades on s.Grade equals gr.Id
                           join se in _context.Sections on s.SectionId equals se.SectionId
                           where s.IsActive == true
                           select new AddStudent
                           {
                               Id = s.Id,
                               FirstName = s.FirstName,
                               LastName = s.LastName,
                               SectionName = se.SectionName,
                               GradeName = gr.ClassName,
                               SectionId = s.SectionId,
                               GradeId = s.Grade,
                               DOB = s.DOB,
                               // DOBString = s.DOB.HasValue ? s.DOB.Value.ToShortDateString() : "",
                               MiddleName = s.MiddleName,
                               IsActive = s.IsActive
                           }).ToList();

                if (GradeId > 0 && SectionId > 0)
                {
                    lstPost = lstPost.Where(x => x.GradeId == GradeId && x.SectionId == SectionId).ToList();
                }
                else if (GradeId > 0)
                {
                    lstPost = lstPost.Where(x => x.GradeId == GradeId).ToList();
                }
                return Json(lstPost);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public JsonResult SaveStudent(AddStudent model)
        {
            var stud = new Student();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Id == Guid.Empty)
                    {
                        stud.Id = Guid.NewGuid();
                        stud.FirstName = model.FirstName;
                        stud.LastName = model.LastName;
                        stud.MiddleName = model.MiddleName;
                        stud.DOB = model.DOB;
                        stud.SectionId = model.SectionId;
                        stud.Grade = model.GradeId;
                        stud.IsActive = true;
                        stud.CreatedBy = "Admin";
                        stud.CreatedDate = System.DateTime.Now;
                        _context.Students.Add(stud);
                        _context.SaveChanges();


                    }
                    else
                    {
                        stud = _context.Students.Where(x => x.Id == model.Id).FirstOrDefault();
                        stud.FirstName = model.FirstName;
                        stud.LastName = model.LastName;
                        stud.MiddleName = model.MiddleName;
                        stud.DOB = model.DOB;
                        stud.SectionId = model.SectionId;
                        stud.Grade = model.GradeId;
                        stud.UpdatedBy = "Admin";
                        stud.UpdatedDate = System.DateTime.Now;
                        _context.Entry(stud).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();

                    }
                    model.Id = stud.Id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(model, JsonRequestBehavior.AllowGet);

        }


        public ActionResult GetGuardianList(string Id)
        {
            try
            {
                Guid studentId = new Guid(Id);
                var guardianList = new List<GuardianViewModel>();

                guardianList = (from s in _context.Students
                                join sg in _context.StudentGuardians on s.Id equals sg.StudentId
                                join g in _context.Guardians on sg.GuardianId equals g.Id
                                where s.Id == studentId
                                select new GuardianViewModel
                                {
                                    Id = g.Id,
                                    FirtName = g.FirtName,
                                    LastName = g.LastName,
                                    StudentName = s.FirstName + " " + s.LastName,
                                    GuardianType = g.GuardianType,
                                    PhoneNumber = g.PhoneNumber

                                }).ToList();

                return Json(guardianList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpPost]
        public JsonResult SaveGuardian(AddGuardian model)
        {
            var guardian = new Guardian();

            var studGuardian = new StudentGuardian();
            //Guid studentId = new Guid(model.GuardianId);
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Id == Guid.Empty)
                    {
                        guardian.Id = Guid.NewGuid();
                        guardian.FirtName = model.FirtName;
                        guardian.LastName = model.LastName;
                        //guardian.MiddleName = model.MiddleName;
                        guardian.PhoneNumber = model.PhoneNumber;
                        //guardian.DeviceId = model.DeviceId;
                        guardian.GuardianType = model.GuardianType;
                        guardian.IsActive = true;
                        guardian.CreatedBy = "Admin";
                        guardian.CreatedDate = System.DateTime.Now;
                        _context.Guardians.Add(guardian);
                        _context.SaveChanges();

                        studGuardian.GuardianId = guardian.Id;
                        studGuardian.StudentId = model.StudentId;
                        _context.StudentGuardians.Add(studGuardian);
                        _context.SaveChanges();


                    }
                    else
                    {
                        guardian = _context.Guardians.Where(x => x.Id == model.Id).FirstOrDefault();
                        //guardian.Id = Guid.NewGuid();
                        guardian.FirtName = model.FirtName;
                        guardian.LastName = model.LastName;
                        //guardian.MiddleName = model.MiddleName;
                        guardian.PhoneNumber = model.PhoneNumber;
                        //guardian.DeviceId = model.DeviceId;
                        guardian.GuardianType = model.GuardianType;
                        guardian.IsActive = true;
                        guardian.UpdatedBy = "Admin";
                        guardian.UpdatedDate = System.DateTime.Now;
                        _context.Entry(guardian).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();

                    }
                    model.Id = guardian.Id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(model, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult LinkGuardian(AddGuardian model)
        {
            var guardian = _context.Guardians.Where(x => x.Id == model.Id).FirstOrDefault();

            var studGuardian = new StudentGuardian();
            //Guid studentId = new Guid(model.GuardianId);
            try
            {
                if (ModelState.IsValid)
                {
                        studGuardian.GuardianId = guardian.Id;
                        studGuardian.StudentId = model.StudentId;
                        _context.StudentGuardians.Add(studGuardian);
                        _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(model, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetGuardiansByGuardianType(string guardianType)
        {
            var lstGuardian = new List<GuardianViewModel>();
            try
            {

                lstGuardian = _context.Guardians.Where(y => y.GuardianType == guardianType).Select(x => new GuardianViewModel
                {
                    Id = x.Id,
                    FirtName = x.FirtName,
                    LastName = x.LastName,
                    PhoneNumber= x.PhoneNumber,
                    GuardianType = x.GuardianType

                }).ToList();
                return Json(lstGuardian, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpPost]
        public ActionResult DeleteStudent(string Id)
        {
            Guid studentId = new Guid(Id);

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _context.Students.Where(x => x.Id == studentId).SingleOrDefault();

            _context.Students.Remove(model);

            int num = _context.SaveChanges();
            return Json(num);
        }

        [HttpPost]
        public ActionResult DeleteGuardian(string GuardianId, string StudentId)
        {

            Guid guardianId = new Guid(GuardianId);
            Guid studentId = new Guid(StudentId);

            if (guardianId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var model = _context.Guardians.Where(x => x.Id == guardianId).FirstOrDefault();

            //_context.Guardians.Remove(model);

            var studentGuardian = _context.StudentGuardians.Where(x => x.GuardianId == guardianId && x.StudentId == studentId).SingleOrDefault();

            _context.StudentGuardians.Remove(studentGuardian);

            int num = _context.SaveChanges();
            return Json(num);
        }

        [HttpPost]
        public ActionResult isGuardianTypeExist(string StudentId, string GuridanType)
        {
                    

            if (StudentId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                Guid StudentGId = new Guid(StudentId);
                var studentList = (from g in _context.Guardians
                                   join sg in _context.StudentGuardians on g.Id equals sg.GuardianId
                                   where sg.StudentId == StudentGId && g.GuardianType == GuridanType
                                   select new { sg.StudentId }).ToList();



                return Json(studentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}