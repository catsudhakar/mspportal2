﻿using MSPPortal.Models;
using MSPPortal.SignalR;
using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using static MSPPortal.Models.GuardisnViewModels;
using System.Data.Entity;
using System.Xml;
using System.Threading;

namespace MSPPortal.Controllers
{
    public class GuardianController : ApiControllerWithHub<PickMeUpHub>
    {
    //    MSPAdminPortalEntities _context = new MSPAdminPortalEntities();
    //    // GET: Guardian

    //    [HttpPut]
    //    public IHttpActionResult GetGuardianId(GuardianViewModel model)
    //    {
    //        try
    //        {
    //            var objGuardian = _context.Guardians
    //                 .FirstOrDefault(x => x.PhoneNumber == model.PhoneNumber);
    //            if (objGuardian != null)
    //            {
    //                 SendOtp(model.PhoneNumber);
    //            }
    //            else
    //            {
    //                return Json("PhNoNotExists");
    //            }
    //            return Json(objGuardian.Id);

    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }
    //    [HttpPost]
    //    public void UpdateDeviceId(GuardianViewModel model)
    //    {
    //        try
    //        {
    //            var objGuardian = _context.Guardians
    //                 .FirstOrDefault(x => x.Id == model.Id);
    //            if (objGuardian != null)
    //            {
    //                objGuardian.DeviceId = model.DeviceId;
    //                _context.SaveChanges();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }

    //    [HttpPost]
    //    public double DistanceCalculate(LatLangViewModel model)
    //    {
    //        double CurrentLattitude = Convert.ToDouble(model.CurrentLattitude);
    //        double CurrentLongitude = Convert.ToDouble(model.CurrentLongitude);

    //        double SchoolLattitude = 17.3946;
    //        double SchoolLongitude = 78.4676;

    //        //double SchoolLattitude = 26.289421;
    //        //double SchoolLongitude = 50.214788;





    //        try
    //        {
          
    //            double distance = Calculate(SchoolLattitude, SchoolLongitude, CurrentLattitude, CurrentLongitude);
    //            if (distance < 220)
    //            {
    //                SendNearByNotification(model.GuardianId, distance);
    //            }
               
    //            return distance;
    //        }
    //        catch (Exception ex)
    //        {
    //            return 0;
    //            throw ex;
    //        }

    //    }
    //    private void SendNearByNotification(Guid guardianId, double distance)
    //    {
    //        var todayDate = DateTime.Now.Date;
    //        var nearByNotification = _context.NearbyNotifications.FirstOrDefault(x => x.GuardianId == guardianId && DbFunctions.TruncateTime(x.NotificationDate) == DbFunctions.TruncateTime(todayDate));
    //        var guardian = _context.Guardians.FirstOrDefault(x => x.Id == guardianId);
    //        var studentList = _context.Students.Where(x => x.GuardianId == guardianId).ToList();
    //        if (nearByNotification == null)
    //        {
    //            if (distance < 50)
    //            {
    //                foreach (var student in studentList)
    //                {
    //                    string message = "Parent <b>" + guardian.FirtName + " " + guardian.LastName + "</b> of " + student.FirstName + " " + student.LastName + " has reached school premises at " + DateTime.Now;
    //                    NearbyNotification notification = SaveNotification(guardian.Id, message, student.Grade, student.SectionId, student.Id);
    //                    var subscribed = Hub.Clients.Group(1.ToString());
    //                    subscribed.addItem1(notification);
    //                }

    //            }
    //            else if (distance < 220)
    //            {
    //                foreach (var student in studentList)
    //                {
    //                    string message = "Parent <b>" + guardian.FirtName + " " + guardian.LastName + "</b> of " + student.FirstName + " " + student.LastName + " coming near to school at" + DateTime.Now;
    //                    NearbyNotification notification = SaveNotification(guardian.Id, message, student.Grade, student.SectionId, student.Id);
    //                    var subscribed = Hub.Clients.Group(1.ToString());
    //                    subscribed.addItem1(notification);
    //                }

    //            }
    //        }
            
    //    }
    //    [HttpPut]
    //    public IHttpActionResult GetStudentsByGuardianId(GuardianViewModel data)
    //    {
    //        var studentList = new List<StudentViewModel>();

    //        var QueueStudentList = new List<StudentViewModel>();
    //        var DissmissStudentList = new List<StudentViewModel>();
    //        var PickedStudentList = new List<StudentViewModel>();


    //        try
    //        {
    //            var todaydate = DateTime.Now.Date.AddDays(-5);


    //            studentList = (from s in _context.Students
    //                           join g in _context.Guardians on s.GuardianId equals g.Id
    //                           join ss in _context.StudentStatus on s.Id equals ss.StudentId
    //                           where ss.CurrentDate >= todaydate & g.Id == data.Id
    //                           select new StudentViewModel
    //                           {
    //                               Id = s.Id,
    //                               FirstName = s.FirstName,
    //                               LastName = s.LastName,
    //                               MiddleName = s.MiddleName,
    //                               FullName = s.FirstName + " " + s.LastName,
    //                               Grade = s.Grade + "Grade",
    //                               Status = (int)ss.Status,
    //                               WaitingTime = ss.WaitingTime.HasValue ? ss.WaitingTime.Value.Hours.ToString() + ":" + ss.WaitingTime.Value.Minutes.ToString() : "",
    //                               QueueTime = ss.QueueTime.HasValue ? ss.QueueTime.Value.Hours.ToString() + ":" + ss.QueueTime.Value.Minutes.ToString() : "",
    //                               PickedUpTime = ss.PickedUpTime.HasValue ? ss.PickedUpTime.Value.Hours.ToString() + ":" + ss.PickedUpTime.Value.Minutes.ToString() : "",
    //                               DismissTime = ss.DismissTime.HasValue ? ss.DismissTime.Value.Hours.ToString() + ":" + ss.DismissTime.Value.Minutes.ToString() : "",
    //                               CurrentDate = ss.CurrentDate,
    //                               GateName = ss.GateId.HasValue ? "Gate" + ss.GateId.ToString() : ""

    //                           }).ToList();


    //            List<DateTime> dateList = new List<DateTime>();
    //            for (int i = 0; i < 5; i++)
    //            {
    //                dateList.Add(DateTime.Now.Date.AddDays(-i));
    //            }

    //            var queuedStudList = dateList.Select(x =>
    //            new StudentsByDateViewModel
    //            {
    //                Date = x.Date.ToShortDateString(),
    //                StudentList = studentList.Where(y => y.CurrentDate == x.Date &&
    //                y.Status == (int)StudentStatus.InQueue).ToList()
    //            }).Where(x => x.StudentList.Count > 0).ToList();

    //            var dismissStudList = dateList.Select(x =>
    //            new StudentsByDateViewModel
    //            {
    //                Date = x.Date.ToShortDateString(),
    //                StudentList = studentList.Where(y => y.CurrentDate == x.Date &&
    //                y.Status == (int)StudentStatus.Dismiss).ToList()
    //            }).Where(x => x.StudentList.Count > 0).ToList();

    //            var pickedStudList = dateList.Select(x =>
    //            new StudentsByDateViewModel
    //            {
    //                Date = x.Date.ToShortDateString(),
    //                StudentList = studentList.Where(y => y.CurrentDate == x.Date &&
    //                y.Status == (int)StudentStatus.PickedUp).ToList()
    //            }).Where(x => x.StudentList.Count > 0).ToList();

    //            Tuple<List<StudentsByDateViewModel>, List<StudentsByDateViewModel>, List<StudentsByDateViewModel>, string, string> tuple =
    //                             new Tuple<List<StudentsByDateViewModel>, List<StudentsByDateViewModel>, List<StudentsByDateViewModel>, string, string>
    //                             (queuedStudList, dismissStudList, pickedStudList, DateTime.Now.ToShortDateString(), DateTime.Now.AddDays(-1).ToShortDateString());



    //            return Json(tuple);
    //        }

    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }

    //    }
    //    public string getSectionName(int GradeId, int SectionId)
    //    {
    //        var sectionname = _context.Sections.Where(x => x.ClassId == GradeId && x.SectionId == SectionId).FirstOrDefault();
    //        return sectionname.SectionName;
    //    }
    //    // POST api/<controller>
    //    [System.Web.Mvc.HttpPost]
    //    public IHttpActionResult UpdateStudentPickedStatus(StudentViewModel obj)
    //    {
    //        try
    //        {

    //            var todaydate = DateTime.Now.Date;
    //            var studentStatus = _context.StudentStatus.FirstOrDefault(x => x.StudentId == obj.Id && x.CurrentDate == todaydate);
    //            if (studentStatus != null)
    //            {
    //                //studentStatus.Status = 4;
    //                studentStatus.Status = (int)StudentStatus.PickedUp;
    //                studentStatus.PickedUpTime = DateTime.Now.TimeOfDay;
    //                studentStatus.UpdatedTime = DateTime.Now;
    //                _context.SaveChanges();
    //            }
    //            var pickedStudent = (from s in _context.Students
    //                                 join ss in _context.StudentStatus on s.Id equals ss.StudentId
    //                                 where ss.CurrentDate == todaydate && ss.Status == (int)StudentStatus.PickedUp && s.Id == obj.Id
    //                                 select new StudentViewModel
    //                                 {
    //                                     Id = s.Id,
    //                                     FirstName = s.FirstName,
    //                                     DOB = s.DOB,
    //                                     Grade = s.Grade,
    //                                     LastName = s.LastName,
    //                                     MiddleName = s.MiddleName,
    //                                     Status = (int)ss.Status,
    //                                     GuardianId = s.GuardianId,
    //                                     Section = s.SectionId,

    //                                 }).FirstOrDefault();



    //            pickedStudent.SectionName = getSectionName(Convert.ToInt16(pickedStudent.Grade), Convert.ToInt16(pickedStudent.Section));





    //            var NewNotification = new Notification();
    //            NewNotification.StudentId = pickedStudent.Id;
    //            NewNotification.GuardianId = pickedStudent.GuardianId;
    //            NewNotification.Message = pickedStudent.FirstName + " " + pickedStudent.LastName + " picked up at " + DateTime.Now;
    //            NewNotification.InStatus = "PickedUp";
    //            NewNotification.NotificationDate = DateTime.Now;
    //            NewNotification.SectionId = Convert.ToInt32(pickedStudent.Section);
    //            NewNotification.GradeId = Convert.ToInt32(pickedStudent.Grade);
    //            NewNotification.NotificationFrom = "Mobile App";
    //            NewNotification.GateId = Convert.ToInt32(obj.GateName.Replace("Gate", ""));
    //            _context.Notifications.Add(NewNotification);
    //            _context.SaveChanges();

    //            Tuple<StudentViewModel, Notification> tuple =
    //                           new Tuple<StudentViewModel, Notification>
    //                           (pickedStudent, NewNotification);


    //            var subscribed = Hub.Clients.Group(1.ToString());
    //            subscribed.addItem(tuple);


    //            return Json(tuple);
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }

    //    }
    //    [HttpPut]
    //    public IHttpActionResult GetNotificationsByGuardianId(GuardianViewModel model)
    //    {
    //        try
    //        {
    //            var notificationList = _context.Notifications.Where(x => x.GuardianId == model.Id).OrderByDescending(x => x.NotificationDate).ToList().Select(x =>
    //                    new NotificationViewModel
    //                    {
    //                        Id = x.Id,
    //                        GateName = "Gate" + x.GateId,
    //                        StudentName = x.Student.FirstName + " " + x.Student.LastName,
    //                        Grade = x.GradeId + "Grade",
    //                        Status = x.InStatus,
    //                        NotificationTime = x.NotificationDate.Value.ToShortTimeString(),
    //                        NotificationDate = x.NotificationDate.Value.ToShortDateString(),
    //                        Message = x.Message

    //                    });
    //            var inQueueNotList = notificationList.Where(x => x.Status == "InQueue").ToList();
    //            var dismissedNotList = notificationList.Where(x => x.Status == "In Dismiss").ToList();
    //            var pickedUpNotList = notificationList.Where(x => x.Status == "PickedUp").ToList();
    //            Tuple<List<NotificationViewModel>, List<NotificationViewModel>, List<NotificationViewModel>> tuple =
    //                            new Tuple<List<NotificationViewModel>, List<NotificationViewModel>, List<NotificationViewModel>>
    //                            (inQueueNotList, dismissedNotList, pickedUpNotList);
    //            return Json(tuple);
    //        }
    //        catch (Exception ex)
    //        {
    //            return Json("error");
    //        }

    //    }

    //    [HttpPost]
    //    public IHttpActionResult AddReportProblem(ReportProblemViewModel model)
    //    {
    //        try
    //        {
    //            var reportProblem = new ReportProblem
    //            {
    //                Name = model.Name,
    //                Message = model.Message,
    //                Email = model.Email,
    //                ReporterId = model.ReporterId

    //            };
    //            _context.ReportProblems.Add(reportProblem);
    //            _context.SaveChanges();

    //            var body = "<p>Hi Admin</p><p><p>You have received feedback from user " + model.Name + "</p>";
    //            body = body + "<p>" + model.Message + "</p><p>Best Regards</p><p>Misk School Team</p>";
    //            var confirmationMsg = "<p>Hi <b>"+model.Name+ "</b>,</p><p>Your request has been sent successfully.We will get back to you soon.</p><p>Best Regards</p><p>Misk School Team</p>";
    //            SendFeedBack("Rammohan.test1@gmail.com", body);
    //            SendFeedBack(model.Email, confirmationMsg);
    //        }
    //        catch (Exception ex)
    //        {
    //            return Json("error");
    //        }
    //        return Json("success");
    //    }
    //    public void SendFeedBack(string email,string body)
    //    {
    //        Thread thread = new Thread(delegate ()
    //        {
    //            var mailMessage = new MailMessage();
    //            mailMessage.To.Add(new MailAddress(email));
    //            //replace with valid value
    //            mailMessage.Subject = "Feedback";
    //            mailMessage.From = new MailAddress("noreply.lrisf@gmail.com", "Misk Schools");
    //            mailMessage.Body = body;
    //            mailMessage.IsBodyHtml = true;
    //            using (var smtp = new SmtpClient())
    //            {

    //                smtp.Host = "smtp.gmail.com";
    //                smtp.EnableSsl = true;
    //                NetworkCredential NetworkCred = new NetworkCredential("noreply.lrisf@gmail.com", "Friday$1");
    //                smtp.UseDefaultCredentials = true;
    //                smtp.Credentials = NetworkCred;
    //                smtp.Port = 587;
    //                smtp.Send(mailMessage);
    //            }
    //        });
    //        thread.IsBackground = true;
    //        thread.Start();
            
            
    //    }
    //    [HttpPost]
    //    public IHttpActionResult VerifyOtp(GuardianViewModel model)
    //    {
    //        var guardian = _context.Guardians.FirstOrDefault(x => x.PhoneNumber == model.PhoneNumber && x.MobileOTP == model.MobileOtp);
    //        if (guardian != null)
    //        {
    //            guardian.IsOtpVerified = true;
    //            _context.SaveChanges();
    //            return Json("success");
    //        }
    //        return Json("error");
    //    }
    //    public NearbyNotification SaveNotification(Guid guardianId, string message,string grade,string sectionId,Guid studentId)
    //    {
    //        var notification = new NearbyNotification
    //        {
    //            GuardianId = guardianId,
    //            Message = message,
    //            NotificationDate = DateTime.Now,
    //            GradeId=Convert.ToInt32(grade),
    //            SectionId=Convert.ToInt32(sectionId),
    //            StudentId=studentId
               
    //        };
    //        _context.NearbyNotifications.Add(notification);
    //        _context.SaveChanges();
    //        return notification;
    //    }
    //    [HttpPost]
    //    public IHttpActionResult UpdateMobileNumber(MobileViewModel model)
    //    {
    //        var guardian = _context.Guardians.FirstOrDefault(x => x.Id == model.GuardianId);
    //        if (guardian != null)
    //        {
    //            guardian.PhoneNumber = model.PhoneNumber;
    //            _context.SaveChanges();
    //            return Json("Success");
    //        }
    //        return Json("Failed");
    //    }
    //    public void SendOtp(string phoneNumber)
    //    {
           
    //        var guardian = _context.Guardians.FirstOrDefault(x => x.PhoneNumber == phoneNumber);
    //        string mobileOtp = string.Empty;
    //        if (guardian != null)
    //        {
    //            phoneNumber = "91"+phoneNumber;
    //            //phoneNumber = phoneNumber.Substring(3);
    //            mobileOtp = GenerateOtp();
    //            string message = "Your password is " + mobileOtp;
    //            WebClient client = new WebClient();
    //            string baseurl = "http://193.105.74.159/api/v3/sendsms/plain?user=kapsystem&password=kap@user!123&sender=KAPNFO&SMSText=" + message + "&GSM=" + phoneNumber;
    //            //string baseurl = "http://193.105.74.159/api/v3/sendsms/plain?user=internationalsms&password=wbBGfCLE&sender=KAPINT&SMSText=" + message + "&type=longsms&GSM="+phoneNumber;
    //            Stream data = client.OpenRead(baseurl);
    //            StreamReader reader = new StreamReader(data);
    //            string ResponseID = reader.ReadToEnd();
    //            data.Close();
    //            reader.Close();
    //            guardian.MobileOTP = mobileOtp;
    //            guardian.IsOtpVerified = false;
    //            _context.SaveChanges();

    //        }
    //    }
    //    protected string GenerateOtp()
    //    {

    //        string numbers = "123456789";
    //        int length = 6;
    //        string otp = string.Empty;
    //        for (int i = 0; i < length; i++)
    //        {
    //            string character = string.Empty;
    //            do
    //            {
    //                int index = new Random().Next(0, numbers.Length);
    //                character = numbers.ToCharArray()[index].ToString();
    //            } while (otp.IndexOf(character) != -1);
    //            otp += character;
    //        }
    //        return otp;
    //    }
    //    public static double Calculate(double lat1, double long1, double lat2, double long2)
    //    {
    //        double R = 6371;
    //        double dLat = ToRad((lat2 - lat1));
    //        double dLon = ToRad((long2 - long1));
    //        lat1 = ToRad(lat1);
    //        lat2 = ToRad(lat2);
    //        double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
    //        double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
    //        double d = R * c;
    //        return d;
    //    }

    //    public static double ToRad(double value)
    //    {
    //        return value * Math.PI / 180;
    //    }
    }
}