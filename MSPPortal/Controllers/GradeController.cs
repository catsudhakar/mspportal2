﻿using MSPPortal.Models;
using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MSPPortal.Controllers
{
    public class GradeController : Controller
    {


        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();

        // GET: Grade
        [Authorize(Roles = "Admin")]
        [Route("gradeList")]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Grade
        [Authorize(Roles = "Admin")]
        [Route("sectionList")]
        public ActionResult Section()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetGradeList()
        {

            var lstPost = new List<GradeViewModel>();
            try
            {

                lstPost = (from g in _context.Grades
                           select new GradeViewModel
                           {
                               GradeId = g.Id,
                               GradeName = g.ClassName
                           }).ToList();

                return Json(lstPost);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult isGradeExist(string Gname)
        {

            var lstGrade = new List<GradeViewModel>();
            try
            {

                lstGrade = _context.Grades.Where(x => x.ClassName == Gname).Select(x => new GradeViewModel
                {
                    GradeId = x.Id,
                    GradeName = x.ClassName
                }).ToList();
                return Json(lstGrade);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpPost]
        public JsonResult AddGrade(GradeViewModel model)
        {
            var grades = new Grade();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.GradeId == 0)
                    {

                        {
                          
                            grades.ClassName = model.GradeName;
                            grades.CreatedBy = "Admin";
                            grades.CreatedDate = DateTime.Now;
                            grades.IsActive = true;

                        };

                        _context.Grades.Add(grades);
                        _context.SaveChanges();

                    }
                    else
                    {
                        grades = _context.Grades.Where(x => x.Id == model.GradeId).SingleOrDefault();

                     
                        grades.ClassName = model.GradeName;
                        grades.UpdatedBy = "Admin";
                        grades.UpdatedDate = DateTime.Now;
                        grades.IsActive = true;

                        _context.Entry(grades).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }

                    model.GradeId = grades.Id;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            // If we got this far, something failed, redisplay form
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSections(string Id)
        {
            var id = Convert.ToInt32(Id);
            var lstPost = new List<SectionViewModel>();
            try
            {

                lstPost = (from g in _context.Grades
                           join se in _context.Sections on g.Id equals se.ClassId
                           where se.ClassId == id
                           select new SectionViewModel
                           {
                               ClassId = g.Id,
                               GradeName = g.ClassName,
                               SectionName = se.SectionName,
                               SectionId = se.SectionId

                           }).ToList();

                return Json(lstPost);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult AddSection(SectionViewModel model)
        {
            var GradeId = Convert.ToInt32(model.ClassId);

            var sections = new Section();
            try
            {
                if (ModelState.IsValid)
                {

                    if (model.SectionId == 0)
                    {

                        {
                            sections.SectionId = model.SectionId;
                            sections.ClassId = GradeId;
                            sections.SectionName = model.SectionName;
                            sections.CreatedBy = "Admin";
                            sections.CreatedDate = DateTime.Now;
                            sections.IsActive = true;
                            sections.GradeName = model.GradeName;

                        };

                        _context.Sections.Add(sections);
                        _context.SaveChanges();

                    }
                    else
                    {
                        sections = _context.Sections.Where(x => x.SectionId == model.SectionId).SingleOrDefault();

                        sections.SectionId = model.SectionId;
                        sections.ClassId = model.ClassId;
                        sections.SectionName = model.SectionName;
                        //sections.CreatedBy = "Admin";
                        //sections.CreatedDate = DateTime.Now;
                        sections.IsActive = true;
                        sections.GradeName = model.GradeName;
                        sections.UpdatedBy = "Admin";
                        sections.UpdatedDate = DateTime.Now;
                        _context.Entry(sections).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }
                }
                model.SectionId = sections.SectionId;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // If we got this far, something failed, redisplay form
            return Json(model, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult DeleteGrade(string Id)
        {
            var id = Convert.ToInt32(Id);

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _context.Grades.Where(x => x.Id == id).FirstOrDefault();

            _context.Grades.Remove(model);

            int num = _context.SaveChanges();
            return Json(num);
        }

        [HttpPost]
        public ActionResult DeleteSection(string Id)
        {
            var id = Convert.ToInt32(Id);

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _context.Sections.Where(x => x.SectionId == id).FirstOrDefault();

            _context.Sections.Remove(model);

            int num = _context.SaveChanges();
            return Json(num);
        }

    }
}