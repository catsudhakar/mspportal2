﻿using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSPPortal.Controllers
{

    [Authorize]
    public class StuedentStatusController : Controller
    {

        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();

        // GET: StuedentStatus
        [Route("studentStatusSummary")]
        public ActionResult Index()
        {
            return View();
        }

        private string getStatusName(int? status)
        {

            string s = string.Empty;
            switch (status)
            {
                case 1:
                    s = "Waiting";
                    break;
                case 2:
                    s = "InQueue";
                    break;
                case 3:
                    s = "Dismiss";
                    break;
                case 4:
                    s = "PickedUp";
                    break;
                default:
                    Console.WriteLine("Invalid grade");
                    break;
            }
            return s;

        }

        [HttpPost]
        public ActionResult GetStudentStatus(int? GradeId, int? SectionId, DateTime? Fromdate, DateTime? Todate, int? Status)
        {
            try
            {

                if (GradeId == 0) GradeId = null;
                if (SectionId == 0) SectionId = null;
                if (Status == 0) Status = null;


                var list = _context.sp_GetStudentStatus(GradeId, SectionId, Status, Fromdate, Todate).ToList();

                //var waitingCount = list.Where(x => x.status == 1).Count();
                //var InqueueCount = list.Where(x => x.status == 2).Count();
                //var DismissCount = list.Where(x => x.status == 3).Count();
                //var pickedCount = list.Where(x => x.status == 4).Count();

                return Json(list);
            }
            catch (Exception ex)
            {
                throw ex;
            }





        }
    }
}