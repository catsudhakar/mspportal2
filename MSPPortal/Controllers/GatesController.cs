﻿using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MSPPortal.Controllers
{
    public class GatesController : Controller
    {

        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();

        // GET: Gates
        [Authorize(Roles = "Admin")]
        [Route("gates")]
        public ActionResult Index()
        {
            return View();
        }

        
        public JsonResult GetAllGate()
        {

            var lstGate = new List<GateViewModel>();
            try
            {

                lstGate = _context.Gates.Select(x => new GateViewModel
                {
                    GateId = x.GateId,
                    GateName = x.GateName
                }).ToList();
                return Json(lstGate);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public JsonResult isNameexist(string Gname)
        {

            var lstGate = new List<GateViewModel>();
            try
            {

                lstGate = _context.Gates.Where(x=>x.GateName== Gname).Select(x => new GateViewModel
                {
                    GateId = x.GateId,
                    GateName = x.GateName
                }).ToList();
                return Json(lstGate);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpPost]
        public JsonResult AddGate(GateViewModel model)
        {
            var gate = new Gate();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.GateId == 0)
                    {

                        {
                           
                            gate.GateName = model.GateName;
                            gate.CreatedBy = "Admin";
                            gate.CreatedDate = DateTime.Now;
                            gate.IsActive = true;

                        };

                        _context.Gates.Add(gate);
                        _context.SaveChanges();

                    }
                    else
                    {
                        gate = _context.Gates.Where(x => x.GateId == model.GateId).SingleOrDefault();

                        gate.GateName = model.GateName;
                        gate.UpdatedBy = "Admin";
                        gate.UpdatedDate = DateTime.Now;
                        

                        _context.Entry(gate).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }
                    model.GateId = gate.GateId;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            // If we got this far, something failed, redisplay form
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteGate(string Id)
        {
            try
            {
                var GateId = Convert.ToInt32(Id);

                if (Id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var model = _context.Gates.Where(x => x.GateId == GateId).FirstOrDefault();

                _context.Gates.Remove(model);

                int num = _context.SaveChanges();
                return Json(num);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}