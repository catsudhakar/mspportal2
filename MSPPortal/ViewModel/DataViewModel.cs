﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSPPortal.ViewModel
{
    public class DataViewModel
    {
        public List<GateViewModel> Gates { get; set; }
        public List<GradeViewModel> Grades { get; set; }
        public List<SectionViewModel> Sections { get; set; }
    }
}