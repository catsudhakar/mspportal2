﻿(function () {
    'use strict';

    angular
        .module('student')
        .controller('linkGuardian', linkGuardian);

    linkGuardian.$inject = ['$scope', '$http', '$mdDialog', '$location', 'toasterService'];

    function linkGuardian($scope, $http, $mdDialog, $location, toasterService) {

        $scope.title = 'Link Guardian';

        var basePath = "/";
        $scope.isDisable = false;

        activate();

        function activate() {
            var i = 0;
            if ($scope.message) {
                $scope.guardian = $scope.message;
            }

        }
        $scope.guardian = {};
        $scope.guardianType = '';

        $scope.getGuadiansByGuardianType = function (guardianType) {

            $scope.isGuardianTypeExist(guardianType);

            $scope.selectedGuardianType = '';
            $scope.Guardians = [];
            $http({
                method: 'POST',
                url: basePath + 'Student/GetGuardiansByGuardianType',
                data: { guardianType: guardianType },
            }).success(function (data) {
                $scope.Guardians = data;
            }).error(function (err) {
                console.log(err);
            });
        }

        $scope.linkGuardian = function (frmGuardian) {
            $scope.isSubmit = true;



            if ($scope.guardian.Id) {
                for (var i = 0; i < $scope.Guardians.length; i++) {
                    if ($scope.Guardians[i].Id === $scope.guardian.Id) {
                        $scope.guardian = $scope.Guardians[i];
                    }
                }
            }
            $scope.guardian.StudentId = window.localStorage.getItem('studentId');

            if (frmGuardian.$valid) {

                $http({
                    method: 'POST',
                    url: basePath + 'Student/LinkGuardian',
                    data: $scope.guardian
                })
               .success(function (result) {
                   if (result) {
                       $mdDialog.hide(result);
                       // toasterService.displaySuccessToaster("Role saved successfully");

                   }
                   else {
                       $mdDialog.hide();
                   }

               }).error(function () {
                   // console.error("error");
               })
            }
        };


        $scope.isGuardianTypeExist = function (guardianType) {

            var studentId = window.localStorage.getItem('studentId');
            $scope.isDisable = false;
            if (guardianType) {

                $http({
                    method: 'POST',
                    url: basePath + 'Student/isGuardianTypeExist',
                    data: { StudentId: studentId, GuridanType: guardianType }
                })
              .success(function (result) {
                  if (result.length > 0) {
                      $scope.isDisable = true;
                      swal({
                          title: "",
                          text: guardianType + " already exist, please select another guardian type",
                          // type: "error",
                          confirmButtonText: "Ok"
                      });
                      //toasterService.displaySuccessDialog("Selected " + guardianType + " already exist please select another type");

                  }
                  else {
                      $scope.isDisable = false;
                  }

              }).error(function () {
                  // console.error("error");
              })


            } //end if
        }
    }
})();
