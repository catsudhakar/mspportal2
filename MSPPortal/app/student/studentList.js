﻿(function () {
    'use strict';

    angular
        .module('student')
        .controller('studentList', studentList);

    studentList.$inject = ['$scope', '$http', '$mdDialog', '$location', 'toasterService'];

    function studentList($scope, $http, $mdDialog, $location, toasterService) {
        $scope.title = 'Students';
        var basePath = "/";
        $scope.grades = [];
        $scope.gates = [];
        $scope.sections = [];

        $scope.students = [];
        $scope.CurrentPage = 0;
        $scope.pageSize = 10;


        activate();

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'Home/GetAllGrade',

            }).success(function (res) {
                if (res !== null) {
                    $scope.grades = res;
                       
                    //$scope.sections = res.Sections;
                    //if (res.Grades.length > 0) {
                    //    $scope.selectedGrade = res.Grades[0].GradeId;
                    //}

                }
            }).error(function (err) {
                console.log(err);
            });

            $http({
                method: 'POST',
                url: basePath + 'Student/GetStudentsList',

            }).success(function (res) {
                if (res !== null) {
                    $scope.students = res;
                }
            }).error(function (err) {
                console.log(err);
            });

        }

        $scope.getSectionByGradeId = function (GradeId) {
            $scope.selectedSection = '';
            $scope.sections = [];
            $http({
                method: 'POST',
                url: basePath + 'Home/GetAllSectionByGradeId',
                data: { Id: JSON.stringify(GradeId) },
            }).success(function (data) {
                $scope.sections = data;
            }).error(function (err) {
                console.log(err);
            });
        }

        $scope.showStudents = function (GradeId, SectionId, frmStudent) {
            $scope.students = '';
            $scope.isSubmit = true;
            if (!SectionId) { SectionId = 0; }
            if (frmStudent.$valid) {

                $http({
                    method: 'POST',
                    url: basePath + 'Student/GetStudentsListByGradeSection',
                    data: { GradeId: JSON.stringify(GradeId), SectionId: JSON.stringify(SectionId) },

                }).success(function (res) {
                    if (res !== null) {
                        $scope.students = res;
                    }
                }).error(function (err) {
                    console.log(err);
                });

            }

        }


        $scope.numberOfPages = function () {
            return Math.ceil($scope.students.length / $scope.pageSize);
        }

        $scope.addStudent = function (ev) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/student/saveStudent.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    message: ev
                }
            })
                .then(function (answer) {
                    activate();
                    toasterService.displaySuccessDialog('Student saved successfully');
                }, function () {

                });
        };

        $scope.editStudent = function (ev, index) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/student/saveStudent.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    message: angular.copy(ev)
                }
            })
                .then(function (answer) {
                    //$scope.students.splice(index, 1);
                    activate();
                    toasterService.displaySuccessDialog('Student updated successfully');
                    //$scope.students.push(answer);



                }, function () {

                });

        };


        $scope.deleteStudent = function (Id, student, grade, section, index) {
            var confirm = $mdDialog.confirm()
                 .title('Do you want to delete "' + student + '" of Grade ' + grade + section + ' ?')
                 .ariaLabel('Do you want to proceed?')
                 .ok('Ok')
                 .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {

                $http({
                        method: 'POST',
                        url: basePath + 'Student/DeleteStudent',
                        data: { 'Id': Id,
                }
                }).success(function (res) {
                    if (res !== null) {
                        // $scope.grades.splice(index, 1);
                        toasterService.displaySuccessDialog(student + ' deleted successfully');
                        activate();
                }
                }).error(function (err) {
                    console.log(err);
                });

            }, function () {

                // window.MyApp.$rootScope.$broadcast("ToasterWarningUpdate",'Cancel');

            });
    };


        function DialogController($scope, $mdDialog, message) {
            $scope.message = message;
            $scope.hide = function () {
                $mdDialog.hide();
        };
            $scope.cancel = function () {
                $mdDialog.cancel();
        };
            $scope.answer = function (answer) {

                $mdDialog.hide(answer);
        };
    }


        $scope.guardians = function (studentId) {

            $location.path('/students/' +studentId);
            //window.location.href = '/students/' + studentId;

    }
}
})();

angular.module('student').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
}
});
