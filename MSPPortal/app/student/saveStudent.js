﻿(function () {
    'use strict';

    angular
        .module('student')
        .controller('saveStudent', saveStudent);

    saveStudent.$inject = ['$scope', '$http', '$mdDialog'];

    function saveStudent($scope, $http, $mdDialog) {
        $scope.title = 'Student';

        if ($scope.message) {
            $scope.title = 'Edit Student';
        }
        else {
            $scope.title = 'Add Student';
        }

        var basePath = "/";
        $scope.grades = [];

        $scope.sections = [];

        $scope.isSubmit = false;

        $scope.student = {};

        $scope.dOB = new Date();

        $scope.myDate = new Date();

        $scope.minDate = new Date(
            $scope.myDate.getFullYear(),
            $scope.myDate.getMonth() - 500,
            $scope.myDate.getDate());
        $scope.maxDate = new Date(
            $scope.myDate.getFullYear(),
            $scope.myDate.getMonth() + 2,
            $scope.myDate.getDate());

        function formatDate(d) {

            var dd = d.getDate()

            if (dd < 10) dd = '0' + dd

            var mm = d.getMonth() + 1
            if (mm < 10) mm = '0' + mm

            var yy = d.getFullYear() % 100
            if (yy < 10) yy = '0' + yy

            //return mm + '/' + dd + '/' + d.getFullYear()
            return d.getFullYear() + '-' + mm + '-' + dd

        }


        activate();

        function activate() {
            var i = 0;
            if ($scope.message) {
                $scope.student = $scope.message;
                i = $scope.student.GradeId;

                if ($scope.student.DOB) {
                   
                    var ticks;

                    // Isolate the numeric portion of the value
                    ticks = /[0-9]+/.exec($scope.student.DOB)[0];

                    // Convert to a number
                    ticks = parseInt(ticks);

                    $scope.myDate = new Date(ticks);
                  
                }
                else {
                    $scope.student.DOB = formatDate($scope.myDate);
                    $scope.myDate = $scope.student.DOB;
                }
                // oldObj = $scope.message;
            }

            $http({
                method: 'POST',
                url: basePath + 'Home/GetDataByGradeId',
                data: { id: i },

            }).success(function (res) {
                if (res !== null) {
                    $scope.grades = res.Grades;
                    $scope.sections = res.Sections;
                    if (res.Grades.length > 0) {
                        if ($scope.message) {
                            //$scope.student.grade = res.Grades[0].GradeId;
                        }
                        else {
                            $scope.student.GradeId = res.Grades[0].GradeId;
                        }

                    }

                }
            }).error(function (err) {

            })
        };

        $scope.getSectionByGradeId = function (GradeId) {
            $scope.selectedSection = '';
            $scope.sections = [];
            $http({
                method: 'POST',
                url: basePath + 'Home/GetSectionByGradeId',
                data: { Id: JSON.stringify(GradeId) },
            }).success(function (data) {
                $scope.sections = data;
            }).error(function (err) {
                console.log(err);
            });
        }

        $scope.addStudent = function (frmStudent) {
            $scope.isSubmit = true;
            $scope.student.DOB = formatDate($scope.myDate);

            $scope.date1 = formatDate($scope.myDate);
            //advData.durationDate = $scope.date1;

            if (frmStudent.$valid) {

                $http({
                    method: 'POST',
                    url: basePath + 'Student/SaveStudent',
                    data: $scope.student
                })
               .success(function (result) {
                   if (result) {
                       $mdDialog.hide(result);
                       // toasterService.displaySuccessToaster("Role saved successfully");
                   }
                   else {
                       $mdDialog.hide();
                   }

               }).error(function () {
                   // console.error("error");
               })
            }
        };
    }
})();
