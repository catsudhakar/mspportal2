﻿(function () {
    'use strict';

    angular
        .module('student')
        .controller('guardian', guardian);

    guardian.$inject = ['$scope', '$http', '$mdDialog', '$location', 'toasterService'];

    function guardian($scope, $http, $mdDialog, $location, toasterService) {
        if ($scope.message) {
            $scope.title = 'Edit Guardian';
        }
        else {
            $scope.title = 'Add Guardian';
        }

        $scope.check = true;

        $scope.isSubmit = false;

        var basePath = "/";

        $scope.isDisable = false;

        activate();

        function activate() {
            var i = 0;
            if ($scope.message) {
                $scope.guardian = $scope.message;
                $scope.guardian.PhoneNumber = parseInt($scope.guardian.PhoneNumber);

            }

        }


        

        function isValid(p) {
            var phoneRe = /^[9][6][6]\d{9}$/;
            //var digits = p.replace(/\D/g, "");

            $scope.check = phoneRe.test(p);
            return phoneRe.test(p);
        }

        $scope.addGuardian = function (frmGuardian) {
        
            var pNo = $scope.guardian.PhoneNumber;
            $scope.check = isValid($scope.guardian.PhoneNumber);

            $scope.isSubmit = true;

            $scope.guardian.StudentId = window.localStorage.getItem('studentId');

            if (frmGuardian.$valid && $scope.check) {

                $http({
                    method: 'POST',
                    url: basePath + 'Student/SaveGuardian',
                    data: $scope.guardian
                })
               .success(function (result) {
                   if (result) {
                       $mdDialog.hide(result);
                       // toasterService.displaySuccessToaster("Role saved successfully");
                   }
                   else {
                       $mdDialog.hide();
                   }

               }).error(function () {
                   // console.error("error");
               })
            }
        };

        $scope.isGuardianTypeExist = function (guardianType) {

            var studentId = window.localStorage.getItem('studentId');
            $scope.isDisable = false;
            if (guardianType) {

                $http({
                    method: 'POST',
                    url: basePath + 'Student/isGuardianTypeExist',
                    data: { StudentId: studentId, GuridanType: guardianType }
                })
              .success(function (result) {
                  if (result.length > 0) {
                      $scope.isDisable = true;

                      swal({
                          title: "",
                          text: guardianType + " already exist, please select another guardian type",
                          // type: "error",
                          confirmButtonText: "Ok"
                      });
                      //toasterService.displaySuccessDialog("Selected " + guardianType + " already exist please select another type");

                  }
                  else {
                      $scope.isDisable = false;
                  }

              }).error(function () {
                  // console.error("error");
              })


            } //end if
        }
    }
})();
