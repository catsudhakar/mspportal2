﻿

(function () {
    'use strict';

    angular
        .module('student')
        .controller('guardianList', guardianList);

    guardianList.$inject = ['$scope', '$http', '$mdDialog', 'toasterService', '$routeParams'];

    function guardianList($scope, $http, $mdDialog, toasterService, $routeParams) {

        $scope.title = 'Student Guardian List';
        $scope.guardians = [];

        $scope.CurrentPage = 0;
        $scope.pageSize = 10;
        var basePath = "/";
        $scope.noRecords = true;
        $scope.message = "No guardians found, please Add or Link Guardians";

        $scope.studentId = $routeParams.studentId;
        sessionStorage.setItem("studentId", $scope.studentId);
        window.localStorage.setItem("studentId", $scope.studentId);
        activate();


        function activate() {
            //var Id = window.localStorage.getItem('studentId');

            $http({
                method: 'POST',
                url: basePath + 'Student/GetGuardianList',
                data: { 'Id': $scope.studentId, }
            }).success(function (res) {
                if (res !== null) {
                    $scope.guardians = res;

                }
            }).error(function (err) {
                console.log(err);
            });


        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.guardians.length / $scope.pageSize);
        }

        $scope.addGuardian = function (ev) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/student/guardian.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: ev
                }
            })
            .then(function (answer) {
                activate();
                toasterService.displaySuccessDialog('Guardian added successfully');
                //$scope.students.push(answer);

            }, function () {

            });
        };

        $scope.editGuardian = function (ev, index) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/student/guardian.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(ev)
                }
            })
            .then(function (answer) {
                activate();
                toasterService.displaySuccessDialog('Guardian updated successfully');

            }, function () {

            });

        };

        $scope.linkGuardian = function (ev) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/student/linkGuardian.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: ev
                }
            })
            .then(function (answer) {
                activate();
                toasterService.displaySuccessDialog('Guardian Linked successfully');
                //$scope.students.push(answer);

            }, function () {

            });
        };


        $scope.deleteGuardian = function (GuardianId, StudentId, name, guardianType, index) {
            var confirm = $mdDialog.confirm()
                 .title('Do you want to delete ' + guardianType + ' "' + name + '"?')
                 .ariaLabel('Do you want to proceed?')
                 .ok('Ok')
                 .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {

                $http({
                    method: 'POST',
                    url: basePath + 'Student/DeleteGuardian',
                    data: { 'GuardianId': GuardianId, 'StudentId': $scope.studentId }
                }).success(function (res) {
                    if (res !== null) {
                        $scope.guardians.splice(index, 1);
                        toasterService.displaySuccessDialog('Guardian deleted successfully');
                    }
                }).error(function (err) {
                    console.log(err);
                });

            }, function () {

                // window.MyApp.$rootScope.$broadcast("ToasterWarningUpdate",'Cancel');

            });
        }

        function DialogController($scope, $mdDialog, message) {
            $scope.message = message;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {

                $mdDialog.hide(answer);
            };
        }

    }
})();

angular.module('student').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

