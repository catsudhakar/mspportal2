﻿

(function () {
    'use strict';

    angular
        .module('gate')
         .controller('gateList', gateList);

    gateList.$inject = ['$scope', '$http', '$mdDialog', 'toasterService'];

    function gateList($scope, $http, $mdDialog, toasterService) {
        $scope.title = 'Gates';
        var basePath = "/";

        $scope.gates = [];



        $scope.CurrentPage = 0;
        $scope.pageSize = 10;


        activate();

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'Gates/GetAllGate',

            }).success(function (res) {
                if (res !== null) {
                    $scope.gates = res;
                }
            }).error(function (err) {
                console.log(err);
            });

        }



        $scope.numberOfPages = function () {
            return Math.ceil($scope.gates.length / $scope.pageSize);
        }

        $scope.addGate = function (ev) {


            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/gates/gate.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: ev
                }
            })
            .then(function (answer) {
                $scope.gates.push(answer);
                toasterService.displaySuccessDialog('Gate saved successfully');;

            }, function () {

            });
        };

        $scope.editGate = function (ev, index) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/gates/gate.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(ev)
                }
            })
            .then(function (answer) {
                activate();
                toasterService.displaySuccessDialog('Gate updated successfully');;

            }, function () {

            });




        };

        $scope.deleteGate = function (Id, gate, index) {
            var confirm = $mdDialog.confirm()
                 .title('Do you want to delete "' + gate + '" ?')
                 .ariaLabel('Do you want to proceed?')
                 .ok('Ok')
                 .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {

                $http({
                    method: 'POST',
                    url: basePath + 'Gates/DeleteGate',
                    data: { 'Id': Id, }
                }).success(function (res) {
                    if (res !== null) {
                        $scope.gates.splice(index, 1);
                        toasterService.displaySuccessDialog(gate + ' deleted successfully');
                    }
                }).error(function (err) {
                    console.log(err);
                });

            }, function () {

                // window.MyApp.$rootScope.$broadcast("ToasterWarningUpdate",'Cancel');

            });
        };

        function DialogController($scope, $mdDialog, message) {
            $scope.message = message;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {

                $mdDialog.hide(answer);
            };
        }
    }
})();

angular.module('gate').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

