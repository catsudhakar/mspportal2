﻿
var gatesModule = angular.module('gate', ['share'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/gates',
            {
                templateUrl: '/app/gates/gateList.html'

            });


        $routeProvider.otherwise({ redirectTo: '/gates' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });





(function (myApp) {
    var gatesService = function ($rootScope, $http, $q, $location, viewModelHelper) {

        var self = this;

        // self.customerId = 0;

        return this;
    };
    myApp.gatesService = gatesService;
}(window.MyApp));


