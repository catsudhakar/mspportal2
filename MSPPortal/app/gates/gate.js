﻿(function () {
    'use strict';

    angular
        .module('gate')
        .controller('saveGate', saveGate);

    saveGate.$inject = ['$scope', '$http', '$mdDialog', 'toasterService'];

    function saveGate($scope, $http, $mdDialog, toasterService) {

        var basePath = "/";
        $scope.gates = [];

        if ($scope.message) {
            $scope.title = 'Edit Gate';
        }
        else {
            $scope.title = 'Add Gate';
        }
        $scope.oldGateName = '';


        $scope.isSubmit = false;
        $scope.gate = {};
        $scope.gateName = '';
        $scope.isGateNameExist = false;

        $scope.gateExist = [];

        activate();

        function activate() {
            if ($scope.message) {
                $scope.gate = $scope.message;
                $scope.oldGateName = $scope.message.GateName;
            }
            else {
                $scope.oldGateName = '';
            }

            $http({
                method: 'POST',
                url: basePath + 'Gates/GetAllGate',

            }).success(function (res) {
                if (res !== null) {
                    $scope.gates = res;
                    for (var i = 0; i < $scope.gates.length; i++) {
                        $scope.gateExist.push($scope.gates[i].GateName);
                    }
                }
            }).error(function (err) {
                console.log(err);
            });
        }

        function isNameExist(gatename) {
            $http({
                method: 'POST',
                url: basePath + 'Gates/isNameExist',
                data: { Gname: gatename },
            }).success(function (res) {
                if (res.length > 0) {
                    $scope.isGateNameExist = true;
                    swal({
                        title: "",
                        text: 'Gate with name ' + gatename + ' already exist',
                        // type: "error",
                        confirmButtonText: "Ok"
                    });
                }
                else {
                    $scope.isGateNameExist = false;
                    swal({
                        title: "",
                        text: 'Gate with name ' + gatename + 'cannot be added. Some error occured.',
                        // type: "error",
                        confirmButtonText: "Ok"
                    });

                }

            }).error(function (err) {
                console.log(err);
            });
        }

        $scope.saveGate = function (frmGate) {

            $scope.isSubmit = true;

            var gname = $scope.gate.GateName;

            if ($scope.gateExist.indexOf(gname) === -1) {
                if (frmGate.$valid) {
                    $http({
                        method: 'POST',
                        url: basePath + 'Gates/AddGate',
                        data: $scope.gate,
                    }).success(function (res) {
                        if (res) {
                            // toasterService.displaySuccessDialog('Gate saved successfully');;
                            $mdDialog.hide(res);

                        }
                        else {
                            $mdDialog.hide();
                        }

                    }).error(function (err) {
                        console.log(err);
                    });
                }

            } else {
                isNameExist(gname);
            }

        };
    }
})();
