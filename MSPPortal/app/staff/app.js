﻿var staffModule = angular.module('staff', ['share'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/staffList',
            {
                templateUrl: '/app/staff/staffList.html'

            });
        $routeProvider.when('/staffDashboard',
           {
               templateUrl: '/app/staff/staffDashboard.html'

           });
      
        $routeProvider.otherwise({ redirectTo: '/staffDashboard' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });





(function (myApp) {
    var staffService = function ($rootScope, $http, $q, $location, viewModelHelper) {

        var self = this;

        // self.customerId = 0;

        return this;
    };
    myApp.staffService = staffService;
}(window.MyApp));


