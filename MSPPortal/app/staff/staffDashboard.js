﻿

(function () {
    'use strict';
    var controllerId = 'staffDashboard';
    angular.module('staff').controller(controllerId,
        ['$scope','$http', '$filter', staffDashboard]);


    function staffDashboard($scope,$http, $filter) {



        var vm = this;
        vm.students = [];
        vm.waitingStudents = [];
        vm.queueStudents = [];
        vm.pickupStudents = [];
        vm.dismissStudents = [];

        $scope.chkwaiting = false;

        $scope.grades = [];
        $scope.gates = [];
        $scope.sections = [];
        $scope.selectedGrade = '';
        $scope.selectedSection = '';
        $scope.selectedTopGate = '';
        $scope.selectedGate = '';
        $scope.isSubmitWaiting = false;

        $scope.isSections = false;

      
        var basePath = "/";

        activate();



        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'Home/GetData',

            }).success(function (res) {
                if (res != null) {
                    $scope.grades = res.Grades;

                    if (res.Grades.length > 0) {
                        $scope.selectedGrade = res.Grades[0].GradeId;
                    }

                    $scope.gates = res.Gates;
                    $scope.sections = res.Sections;

                }
            }).error(function (err) {
                console.log(err);
            });

            
        }




        $scope.getSectionByGradeId = function (GradeId) {
            $scope.selectedSection = '';
            $scope.sections = [];
            if (GradeId ==0) {
                $scope.isSections = true;

            }
            else {
                $scope.isSections = false
                
                $http({
                    method: 'POST',
                    url: basePath+'Home/GetSectionByGradeId',
                    data: { Id: JSON.stringify(GradeId) },
                }).success(function (data) {
                    $scope.sections = data;
                }).error(function (err) {
                    console.log(err);
                });
            }

            



        }

        $scope.getStudents = function () {

            
            vm.waitingStudents = [];
            vm.queueStudents = [];
            vm.pickupStudents = [];
            vm.dismissStudents=[];


            $http({
                method: 'POST',
                url: basePath + 'Staff/GetStudents',
                data: { GradeId: "", SecId: "" },
            }).success(function (data) {
                vm.waitingStudents = data.Item1;
                vm.queueStudents = data.Item2;
                vm.dismissStudents = data.Item3;
                vm.pickupStudents = data.Item4;
                setNanoScroll();
            }).error(function (err) {
                console.log(err);
            });
        }


        $scope.getStudentsByGrade = function (gradeId, sectionId, form) {
            $scope.isSubmitWaiting = true;
            vm.waitingStudents = [];
            vm.queueStudents = [];
            vm.pickupStudents = [];
            vm.dismissStudents = [];

            if (form.$valid && $scope.isSubmitWaiting) {

                $http({
                    method: 'POST',
                    url: basePath+'Staff/GetStudents',
                    data: { GradeId: gradeId, SecId: sectionId },
                }).success(function (data) {
                    vm.waitingStudents = data.Item1;
                    vm.queueStudents = data.Item2;
                    vm.dismissStudents = data.Item3;
                    vm.pickupStudents = data.Item4;
                    setNanoScroll();
                }).error(function (err) {
                    console.log(err);
                });
            }
        };







        function setNanoScroll() {
            setTimeout(function () {
                $('.nano').nanoScroller({
                    preventPageScrolling: true,
                    alwaysVisible: true
                });
            }, 100);
        }



        //$scope.waitingnumberOfPages = function () {
        //    return Math.ceil(vm.waitingStudents.length / $scope.waitingpageSize);
        //}





    };
})();

angular.module('staff').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});