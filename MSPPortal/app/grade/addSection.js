﻿(function () {
    'use strict';

    angular
        .module('grade')
        .controller('addSection', addSection);

    addSection.$inject = ['$scope', '$http', '$mdDialog', 'toasterService'];

    function addSection($scope, $http, $mdDialog, toasterService) {

        var basePath = "/";

        $scope.sectionList = [];
        $scope.sectionExist = [];
        $scope.title = 'Section';

        if ($scope.message) {
            $scope.title = 'Edit Section';
        }
        else {
            $scope.title = 'Add Section';
        }
        $scope.isSubmit = false;
        $scope.gradeId = '';

        $scope.section = {};

        activate();

        function activate() {
            var i = 0;
            if ($scope.message) {
                $scope.section = $scope.message;
                i = $scope.section.GradeId;

            }
            var Id;
            var i = 0;

            $scope.gradeId = window.localStorage.getItem('gradeId');
            i = $scope.gradeId;
            Id = $scope.gradeId;

            $http({
                method: 'POST',
                url: basePath + 'Grade/GetSections',
                data: {
                    'Id': Id
                }
            })
               .success(function (result) {
                   if (result) {
                       $scope.sectionList = result;
                       for (var i = 0; i < $scope.sectionList.length; i++) {

                           $scope.sectionExist.push($scope.sectionList[i].SectionName);

                       }
                   }
                   else {
                       $mdDialog.hide();
                   }

               }).error(function () {
                   // console.error("error");
               })
        }


        $scope.addSection = function (frmStudent) {
            $scope.isSubmit = true;



            var sectionName = $scope.section.SectionName;

            if ($scope.sectionExist.indexOf(sectionName) === -1) {

                if (!$scope.section.ClassId) {
                    var classId = window.localStorage.getItem('gradeId');
                    var gradeName = window.localStorage.getItem('gradeName')

                    $scope.section.ClassId = classId;
                    $scope.section.GradeName = gradeName;
                }

                if (frmStudent.$valid) {

                    $http({
                        method: 'POST',
                        url: basePath + 'Grade/AddSection',
                        data: $scope.section
                    })
                   .success(function (result) {
                       if (result) {
                           //toasterService.displaySuccessDialog('Section Edited successfully');
                           $mdDialog.hide(result);
                       }
                       else {
                           $mdDialog.hide();
                       }

                   }).error(function () {
                       // console.error("error");
                   })
                }
            } else {
                swal({
                    title: "",
                    text: 'Section with name "' + sectionName + '" already exist',
                    // type: "error",
                    confirmButtonText: "Ok"
                });
            };
        }


    }
})();
