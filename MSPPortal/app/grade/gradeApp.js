﻿
var gradeModule = angular.module('grade', ['share'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/gradeList',
            {
                templateUrl: '/app/grade/gradeList.html'

            });
        $routeProvider.when('/sectionList',
           {
               templateUrl: '/app/grade/sections.html'

           });

        $routeProvider.otherwise({ redirectTo: '/gradeList' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });





(function (myApp) {
    var gradeService = function ($rootScope, $http, $q, $location, viewModelHelper) {

        var self = this;

        // self.customerId = 0;

        return this;
    };
    myApp.gradeService = gradeService;
}(window.MyApp));


