﻿(function () {
    'use strict';

    angular
        .module('grade')
        .controller('sections', saveGrade);

    saveGrade.$inject = ['$scope', '$http', '$mdDialog', 'toasterService'];

    function saveGrade($scope, $http, $mdDialog, toasterService) {

        var basePath = "/";

        $scope.title = 'Sections';

        $scope.isSubmit = false;
        $scope.CurrentPage = 0;
        $scope.pageSize = 10;


        $scope.section = {};

        $scope.gradeId = 0;

        $scope.sectionList = [];

        $scope.noRecords = true;
        $scope.message = "No sections found, please Add Section";

        activate();
        var Id;

        function activate() {

            var i = 0;

            $scope.gradeId = window.localStorage.getItem('gradeId');
            i = $scope.gradeId;
            Id = $scope.gradeId;

            $http({
                method: 'POST',
                url: basePath + 'Grade/GetSections',
                data: {
                    'Id': Id
                }
            })
               .success(function (result) {
                   if (result) {
                       $scope.sectionList = result;
                   }
                   else {
                       $mdDialog.hide();
                   }

               }).error(function () {
                   // console.error("error");
               })

        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.grades.length / $scope.pageSize);
        }


        $scope.addSection = function (ev) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/grade/addSection.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: ev
                }
            })
            .then(function (answer) {

                $scope.sectionList.push(answer);
                toasterService.displaySuccessDialog('Section saved successfully');

            }, function () {

            });
        };

        $scope.editSection = function (ev, index) {


            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/grade/addSection.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(ev)
                }
            })
            .then(function (answer) {
                $scope.sectionList.splice(index, 1);
                $scope.sectionList.push(answer);
                toasterService.displaySuccessDialog('Section edited successfully');

            }, function () {

            });

        };

        $scope.deleteSection = function (Id, grade, section, index) {
            var confirm = $mdDialog.confirm()
                 .title('Do you want to delete "' + grade + ' - Section (' + section + ')" ?')
                 .ariaLabel('Do you want to proceed?')
                 .ok('Ok')
                 .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {

                $http({
                    method: 'POST',
                    url: basePath + 'Grade/DeleteSection',
                    data: { 'Id': Id, }
                }).success(function (res) {
                    if (res !== null) {
                        activate();
                        toasterService.displaySuccessDialog(grade + ' - Section (' + section + ') deleted successfully');
                    }
                }).error(function (err) {
                    //console.log(err);
                    toasterService.displaySuccessDialog(grade + ' - Section (' + section + ') cannot be deleted, students are associated with it.');
                });

            }, function () {

                // window.MyApp.$rootScope.$broadcast("ToasterWarningUpdate",'Cancel');

            });
        };


        function DialogController($scope, $mdDialog, message) {
            $scope.message = message;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {

                $mdDialog.hide(answer);
            };
        }
    }
})();


