﻿(function () {
    'use strict';

    angular
        .module('grade')
        .controller('gradeList', gradeList);

    gradeList.$inject = ['$scope', '$http', '$mdDialog', 'toasterService'];

    function gradeList($scope, $http, $mdDialog, toasterService) {
        $scope.title = 'Grades';
        var basePath = "/";
        $scope.grades = [];

        $scope.grade = [];
        $scope.CurrentPage = 0;
        $scope.pageSize = 10;


        activate();

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'Grade/GetGradeList',

            }).success(function (res) {
                if (res !== null) {
                    $scope.grades = res;
                }
            }).error(function (err) {
                console.log(err);
            });

        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.grades.length / $scope.pageSize);
        }

        $scope.addGrade = function (ev) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/grade/saveGrade.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: ev
                }
            })
            .then(function (answer) {

                toasterService.displaySuccessDialog('Grade saved successfully');
                activate();

            }, function () {

            });
        };

        $scope.editGrade = function (ev, index) {


            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/grade/saveGrade.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(ev)
                }
            })
            .then(function (answer) {
                $scope.grades.push(answer);
                toasterService.displaySuccessDialog('Grade updated successfully');
                activate();

            }, function () {

            });

        };

        $scope.sections = function (ev, index) {

            window.localStorage.setItem('gradeId', ev.GradeId);
            window.localStorage.setItem('gradeName', ev.GradeName);
            window.location.href = '/sectionList';
        };

        $scope.deleteGrade = function (Id, grade, index) {
            var confirm = $mdDialog.confirm()
                 .title('Do you want to delete "' + grade + '" ?')
                 .ariaLabel('Do you want to proceed?')
                 .ok('Ok')
                 .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {

                $http({
                    method: 'POST',
                    url: basePath + 'Grade/DeleteGrade',
                    data: { 'Id': Id, }
                }).success(function (res) {
                    if (res !== null) {
                        // $scope.grades.splice(index, 1);
                        toasterService.displaySuccessDialog(grade + ' deleted successfully');
                        activate();
                    }
                }).error(function (err) {
                    //console.log(err);
                    toasterService.displaySuccessDialog(grade + ' cannot be deleted, students are associated with it.');
                });

            }, function () {

                // window.MyApp.$rootScope.$broadcast("ToasterWarningUpdate",'Cancel');

            });
        };


        //$scope.addSection = function (ev) {

        //    $mdDialog.show({
        //        controller: DialogController,
        //        templateUrl: '/app/grade/addSection.html',
        //        parent: angular.element(document.body),
        //        targetEvent: ev,
        //        clickOutsideToClose: false,
        //        locals: {
        //            message: ev
        //        }
        //    })
        //    .then(function (answer) {

        //        $scope.grade.push(answer);

        //    }, function () {

        //    });
        //};

        //$scope.editSection = function (ev, index) {


        //    $mdDialog.show({
        //        controller: DialogController,
        //        templateUrl: '/app/grade/addSection.html',
        //        parent: angular.element(document.body),
        //        targetEvent: ev,
        //        clickOutsideToClose: false,
        //        locals: {
        //            message: angular.copy(ev)
        //        }
        //    })
        //    .then(function (answer) {
        //        $scope.grades.splice(index, 1);
        //        $scope.grades.push(answer);

        //    }, function () {

        //    });

        //};


        function DialogController($scope, $mdDialog, message) {
            $scope.message = message;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {

                $mdDialog.hide(answer);
            };
        }
    }
})();

angular.module('grade').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
