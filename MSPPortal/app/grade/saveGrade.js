﻿(function () {
    'use strict';

    angular
        .module('grade')
        .controller('saveGrade', saveGrade);

    saveGrade.$inject = ['$scope', '$http', '$mdDialog', 'toasterService'];

    function saveGrade($scope, $http, $mdDialog, toasterService) {

        var basePath = "/";

        $scope.title = 'Grade';

        $scope.grades = [];

        $scope.gradeExist = [];

        if ($scope.message) {
            $scope.title = 'Edit Grade';
        }
        else {
            $scope.title = 'Add Grade';
        }
        $scope.isSubmit = false;


        $scope.grade = {};

        activate();

        function activate() {
            var i = 0;
            if ($scope.message) {
                $scope.grade = $scope.message;
                i = $scope.grade.GradeId;
            }

            $http({
                method: 'POST',
                url: basePath + 'Grade/GetGradeList',

            }).success(function (res) {
                if (res !== null) {
                    $scope.grades = res;
                    for (var i = 0; i < $scope.grades.length; i++) {
                        $scope.gradeExist.push($scope.grades[i].GradeName);
                    }
                }
            }).error(function (err) {
                console.log(err);
            });
        }

        function isGradeExist(gradeName) {
            $http({
                method: 'POST',
                url: basePath + 'Grade/isGradeExist',
                data: { Gname: gradeName }

            }).success(function (res) {
                if (res.length > 0 ) {
                    swal({
                        title: "",
                        text: 'Grade with name "' + gradeName + '" already exist',
                        // type: "error",
                        confirmButtonText: "Ok"
                    });
                }
            }).error(function (err) {
                //console.log(err);
                swal({
                    title: "",
                    text: 'Grade with name "' + gradeName + '" cannot be added, some error occured : ' + err,
                    // type: "error",
                    confirmButtonText: "Ok"
                });
            });
        }

        $scope.addGrade = function (frmGrade) {
            $scope.isSubmit = true;

            var gradeName = $scope.grade.GradeName;

            if ($scope.gradeExist.indexOf(gradeName) === -1) {
                if (frmGrade.$valid) {

                    $http({
                        method: 'POST',
                        url: basePath + 'Grade/AddGrade',
                        data: $scope.grade
                    })
                   .success(function (result) {
                       if (result) {

                           $mdDialog.hide(result);
                           //toasterService.displaySuccessDialog('Grades updated successfully');
                       }
                       else {
                           $mdDialog.hide();
                       }

                   }).error(function () {
                       // console.error("error");
                   })
                }
            }
            else {
                isGradeExist(gradeName);
            }

        };
    }
})();
