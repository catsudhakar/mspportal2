﻿
(function () {
    'use strict';

    angular
       .module('studentStatus')
       .controller('studentStatusReport', studentStatusReport);

    studentStatusReport.$inject = ['$scope', '$http'];

    function studentStatusReport($scope, $http) {


        $scope.notifications = [];
        $scope.grades = [];

        $scope.sections = [];
        $scope.selectedGrade = '';
        $scope.selectedSection = '';

        $scope.isSubmitWaiting = false;

        $scope.CurrentPage = 0;
        $scope.pageSize = 12;
        $scope.noRecords = true;
        $scope.message = "Please select grade,section,status and date range to see the Student status summary.";

        //$scope.startDate = new Date();
        //$scope.endDate = new Date();
        var basePath = "/";
        activate();

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'Home/GetAllGrade',

            }).success(function (res) {
                if (res !== null) {
                    $scope.grades = res;

                }
            }).error(function (err) {
                console.log(err);
            });

            //var backColor = '';
            //var dataPoints = '[';
            //for (var i = 0; i < 2; i++) {
            //    var comma = ',';
            //    if (i == 0) {
            //        backColor = '#F1C795 #feebd2';
            //    } else if (i == 1) {
            //        backColor = '##FA6E6E #FA9494';
            //    } else {
            //        backColor = '#D2D6DE';
            //    }

            //    if (i === 2 - 1) {
            //        comma = '';
            //    }

            //    dataPoints += '{text:"Total Commits",values:[4660],backgroundColor:"#FA6E6E #FA9494"}' + comma + ''

            //}

            //dataPoints += ']'

            //var dps = dataPoints;

            //// var myobj = JSON.parse(dataPoints);

            //var str = '{a:"www"}';
            //var obj = eval("(" + dataPoints + ")");
            ////console.log(obj);

            //var s = [{ text: 'Total Commits', values: [4660], backgroundColor: '#FA6E6E #FA9494' },
            //{ text: 'Total Commits', values: [4660], backgroundColor: '#FA6E6E #FA9494' }];


            //var ss = [{ text: "Total Commits", values: [4660], backgroundColor: "#FA6E6E #FA9494" },
            //{ text: "Total Commits", values: [4660], backgroundColor: "#FA6E6E #FA9494" }];



        }



        $scope.exportReport = function () {

            //http://jsfiddle.net/95j0txwx/1132/
            if ($scope.notifications.length > 0) {
                alasql('SELECT StudentName, CurrentDate As Date,StatusName As Status,WaitingTime as [In Waiting],QueueTime as [In Queue],DismissTime as Dismissed,PickedUpTime as [Picked up],GuardianName as [Picked by],GradeName,SectionName INTO XLSX("StudentStatusReport.xlsx",{headers:true}) FROM ?', [$scope.notifications]);
            }

        }

        $scope.getReport = function (GradeId, SectionId, StartDate, EndDate, form, status) {
            $scope.isSubmitWaiting = true;
            $scope.noRecords = false;
            $scope.CurrentPage = 0;
            $scope.pageSize = 12;
            $scope.isSubmitWaiting = true;
            $scope.isgreaterdate = false;
            $scope.notifications = [];
            if (form.$valid && $scope.isSubmitWaiting) {
                if (EndDate < StartDate) {
                    $scope.isgreaterdate = true;
                }

                if (!$scope.isgreaterdate) {
                    $http({
                        method: 'POST',
                        url: basePath + 'StuedentStatus/GetStudentStatus',
                        data: {
                            GradeId: GradeId,
                            SectionId: SectionId,
                            Fromdate: StartDate,
                            Todate: EndDate,
                            Status: status
                        },
                    }).success(function (data) {
                        $scope.notifications = data;



                        if ($scope.notifications.length == 0) {
                            $scope.noRecords = true;
                            $scope.message = "No records found.";
                        }
                        else {
                            var waitingCount = _.where(data, { status: 1 }).length;
                            var queueCount = _.where(data, { status: 2 }).length;
                            var dismissCount = _.where(data, { status: 3 }).length;
                            var pickupCount = _.where(data, { status: 4 }).length;
                            var comma = ',';

                            var dataPoints = '[';
                            if (waitingCount > 0)
                                dataPoints += '{text:"Waiting",values:[' + waitingCount + '],backgroundColor:"#4285F4"}' + comma + ''
                            if (queueCount > 0)
                                dataPoints += '{text:"In Queue",values:[' + queueCount + '],backgroundColor:"#EA4335"}' + comma + ''
                            if (dismissCount > 0)
                                dataPoints += '{text:"Dismissed",values:[' + dismissCount + '],backgroundColor:"#FA6E6E"}' + comma + ''
                            if (pickupCount > 0)
                                dataPoints += '{text:"PickedUp",values:[' + pickupCount + '],backgroundColor:"#34A853"}' + comma + ''

                             dataPoints += ']'

                             var obj = eval("(" + dataPoints + ")");
                           

                            $scope.myJson = {
                                globals: {
                                    shadow: false,
                                    fontFamily: "Verdana",
                                    fontWeight: "100"
                                },
                                type: "pie",
                                backgroundColor: "#fff",

                                legend: {
                                    layout: "x5",
                                    position: "50%",
                                    borderColor: "transparent",
                                    marker: {
                                        borderRadius: 10,
                                        borderColor: "transparent"
                                    }
                                },
                                tooltip: {
                                    text: "%v %t"
                                },
                                plot: {
                                    refAngle: "-90",
                                    borderWidth: "0px",
                                    valueBox: {
                                        placement: "in",
                                        // text: "%v %t",
                                        text: "%v",
                                        fontSize: "15px",
                                        textAlpha: 1,
                                    }
                                },
                                series:obj
                                //series: [{ text: 'Waiting', values: [waitingCount], backgroundColor: '#4285F4' },
                                //    { text: 'In Queue', values: [queueCount], backgroundColor: '#EA4335' },
                                //{ text: 'Dismissed', values: [dismissCount], backgroundColor: '#FA6E6E' },
                                //{ text: 'PickedUp', values: [pickupCount], backgroundColor: '#34A853' }]
                            };
                        }

                    }).error(function (err) {
                        console.log(err);
                    });
                }
            }

        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.notifications.length / $scope.pageSize);
        }


        $scope.getSectionByGradeId = function (GradeId) {
            $scope.selectedSection = '';
            $scope.sections = [];
            $http({
                method: 'POST',
                url: basePath + 'Home/GetAllSectionByGradeId',
                data: {
                    Id: JSON.stringify(GradeId)
                },
            }).success(function (data) {
                $scope.sections = data;
            }).error(function (err) {
                console.log(err);
            });



        }
    }
})();

angular.module('studentStatus').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

