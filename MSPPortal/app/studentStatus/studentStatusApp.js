﻿
var studentStatusModule = angular.module('studentStatus', ['share', 'zingchart-angularjs'])
    .config(function ($routeProvider, $locationProvider) {

        $routeProvider.when('/studentStatusSummary',
            {
                templateUrl: '/app/studentStatus/studentStatusReport.html'

            });
        

        $routeProvider.otherwise({ redirectTo: '/studentStatusSummary' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });





(function (myApp) {
    var studentStatusService = function ($rootScope, $http, $q, $location, viewModelHelper) {

        var self = this;

        // self.customerId = 0;

        return this;
    };
    myApp.studentStatusService = studentStatusService;
}(window.MyApp));


