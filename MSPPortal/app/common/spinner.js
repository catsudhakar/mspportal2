(function () {
    'use strict';
    // Must configure the common service and set its 
    // events via the commonConfigProvider
    angular.module('common')
        .factory('spinner', ['common', 'commonConfig', spinner]);
    function spinner(common, commonConfig) {
        var service = {
            spinnerHide: spinnerHide,
            spinnerShow: spinnerShow
        };
        return service;
        function spinnerHide() { spinnerToggle(false); }
        function spinnerShow(msg) { spinnerToggle(true,msg); }
        function spinnerToggle(show,msg) {
            common.$broadcast(commonConfig.config.spinnerToggleEvent, { show: show,msg:msg });
        }
    }
})();