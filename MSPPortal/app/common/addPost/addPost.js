﻿(function () {
    'use strict';

    angular
        .module('main')
        .controller('addPostController', addPost);

    addPost.$inject = ['$scope', 'viewModelHelper', '$http', '$mdDialog', '$location', '$rootScope', 'toasterService', '$mdBottomSheet', '$translate', 'spinner'];

    function addPost($scope, viewModelHelper, $http, $mdDialog, $location, $rootScope, toasterService, $mdBottomSheet, $translate, spinner) {
        var categoryId = sessionStorage.getItem("categoryId");
        var sectionId = sessionStorage.getItem("sectionId");
        var sectionName = sessionStorage.getItem("sectionName");
        $scope.sectionId = parseInt(sectionId);
        $scope.sectionName = sectionName;
        $scope.isSubmit = false;
        var relatedPostId = 0;
        var self = this;
        $scope.querySearch = querySearch;
        loadKeywords();
        $scope.filterSelected = true;
        $scope.disableSave = false;

        $scope.hideLoader = true;

        function querySearch(query) {
            var results = (query || query === '') ?
                $scope.allKeywords.filter(createFilterFor(query)) : $scope.allKeywords;

            for (var i = 0; i < $scope.post.keywords.length; i++) {

                for (var i2 = 0; i2 < results.length; i2++) {

                    if (results[i2].name.toLowerCase() == $scope.post.keywords[i].name.toLowerCase()) {

                        results.splice(i2, 1);

                    }

                }

            }
            return results;
        }

        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(contact) {
                return (contact.name.toLowerCase().indexOf(lowercaseQuery) != -1);;
            };
        }
        window.MyApp.$rootScope.$on("languageChanged", function (event, data) {
            $translate.use(data);
        });
        function loadKeywords() {
            var keywords = [];

            viewModelHelper.apiPost('Home/GetKeywordsByCategoryId', '"' + categoryId + '"',
            function (res) {
                $scope.allKeywords = res.data;
                self.keywords = [$scope.allKeywords[0]];
            },
            function (err) {
                console.log(err);
            });


        }

        $scope.save = function (addPostForm) {
            $scope.isSubmit = true;
            $scope.isKeywords = false;
            if ($scope.post.keywords.length == 0) {
                $scope.isKeywords = true;
            }
            if (addPostForm.$valid && $scope.isKeywords == false) {
                $scope.disableSave = true;
                var image_Video = '';
                var reader = new window.FileReader();
                reader.readAsDataURL($scope.file);
                reader.onloadend = function () {
                    image_Video = reader.result.split(',')[1];
                    $scope.post.src = image_Video;
                    $scope.post.fileType = $scope.file.type;
                    $scope.post.categoryId = categoryId;
                    $scope.post.sectionId = sectionId;
                    $scope.post.location = MyApp.location;
                    if ($scope.post.relatedContent) {
                        $scope.post.relatedPostId = relatedPostId;
                    }
                    $scope.hideLoader = false;
                    $scope.progressMode = "indeterminate";
                    spinner.spinnerShow("Please wait...");
                    viewModelHelper.apiPost('Home/AddPost', $scope.post,
                    function (res) {
                        $scope.progressMode = "";
                        $mdDialog.hide();
                        spinner.spinnerHide();
                        $translate('Layout.PostAdded').then(function (translation) {
                            toasterService.displaySuccessToaster(translation);
                        });
                        if (window.location.pathname === "/Manage") {
                            $rootScope.$broadcast("newPost", res.data);
                        }
                    },
                   function (err) {
                       spinner.spinnerHide();
                       //console.log(err);
                   });

                }
            }
        }
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.createChip = function (_val) {
            if (typeof _val === 'string') {
                return {
                    name: _val,
                    id: "F586EEC7-91C9-454D-97F6-346ADB8E8D79"
                };
            }
            else {
                return _val;
            }
        }
        $scope.addRelatedContent = function () {
            if (!$scope.post.relatedContent) {
                sessionStorage.setItem("postId", 0);
                sessionStorage.setItem("sectionId", sectionId);

                $mdBottomSheet.show({
                    templateUrl: '/app/common/addPost/addMultipleRelatedItems.html',
                    clickOutsideToClose: false,


                }).then(function (data) {
                    if (data) {
                        relatedPostId = data.id;
                    }
                    else {
                        $scope.post.relatedContent = false;
                    }

                }, function () {
                    $scope.post.relatedContent = false;
                });
            }
        }

    }
})();
