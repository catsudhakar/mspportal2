﻿(function () {
    'use strict';

    angular
        .module('notification')
        .controller('notificationsList', notificationsList);
      
    

    notificationsList.$inject = ['$scope', '$http', '$sce'];

    function notificationsList($scope, $http, $sce) {
        $scope.title = 'Notifications History';

        $scope.isSubmitWaiting = false;
        $scope.isSectionSelected = false;

        $scope.notifications = [];
        $scope.grades = [];

        $scope.sections = [];
        $scope.selectedGrade = '';
        $scope.selectedSection = '';

        $scope.CurrentPage = 0;
        $scope.pageSize = 12;

        //$scope.startDate = new Date();
        //$scope.endDate = new Date();
        var basePath = "/";
        $scope.message = "Please select grade,section,status and date range to see the notification history.";
        $scope.noRecords = true;
        activate();

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'Home/GetAllGrade',

            }).success(function (res) {
                if (res !== null) {
                    //res. GradeId = x.Id,
                    //GradeName = x.ClassName
                    $scope.grades = res;

                }
            }).error(function (err) {
                console.log(err);
            });
           
        }

        function formatDate(d) {

            var dd = d.getDate()

            if (dd < 10) dd = '0' + dd

            var mm = d.getMonth() + 1
            if (mm < 10) mm = '0' + mm

            var yy = d.getFullYear() % 100
            if (yy < 10) yy = '0' + yy

            return mm + '/' + dd + '/' + d.getFullYear()

        }

        alasql.fn.myfn = function (str) {
            return str.replace(/<[\/]{0,1}(b)[^><]*>/g, "");
           
        }

        $scope.exportReport = function () {
            if ($scope.notifications.length > 0) {
                alasql('SELECT StudentName, GuardianName,Status, myfn(Message) as Message,NotificationDate,GradeName,SectionName INTO XLSX("exportNotification.xlsx",{headers:true}) FROM ?', [$scope.notifications]);
            }
        }

        $scope.getReport = function (GradeId, SectionId, StartDate, EndDate, form, status) {

            $scope.isSubmitWaiting = true;
            $scope.isSectionSelected = true;
            $scope.noRecords = false;
            //var sd = Date.parse(StartDate);
            $scope.notifications = [];
            
            $scope.isgreaterdate = false;
            $scope.CurrentPage = 0;
            $scope.pageSize = 12;
            

            var startDateString = '';
            var endDateString = '';

            if (form.$valid && $scope.isSubmitWaiting) {
                 

                if (EndDate < StartDate) {
                    $scope.isgreaterdate = true;
                }

                if (StartDate) {
                    startDateString = formatDate(StartDate);
                }
                if (EndDate) {
                    endDateString = formatDate(EndDate);
                }
                if (!$scope.isgreaterdate) {
                    $http({
                        method: 'POST',
                        url: basePath+'Notification/GetNotification',
                        data: {
                            GradeId: GradeId,
                            SectionId: SectionId,
                            Fromdate: startDateString,
                            Todate: endDateString,
                            Status: status
                        },
                    }).success(function (data) {
                        $scope.notifications = data;
                        if ($scope.notifications.length == 0) {
                            $scope.noRecords = true;
                            $scope.message = "No records found.";
                        }

                    }).error(function (err) {
                        console.log(err);
                    });
                }
            }

        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.notifications.length / $scope.pageSize);
        }


        $scope.getSectionByGradeId = function (GradeId) {
            $scope.selectedSection = '';
            $scope.sections = [];
            $http({
                method: 'POST',
                url: basePath +'Home/GetAllSectionByGradeId',
                data: {
                    Id: JSON.stringify(GradeId)
                },
            }).success(function (data) {
                $scope.sections = data;
            }).error(function (err) {
                console.log(err);
            });



        }

        $scope.encodeHtml = function (inputString) {
            return $sce.trustAsHtml(inputString);
        };
    }
})();

angular.module('notification').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

