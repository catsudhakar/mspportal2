﻿(function () {
    'use strict';

    angular
        .module('home')
        .controller('busStudentsCtrl', busStudents);

    busStudents.$inject = ['$scope']; 

    function busStudents($scope) {
        $scope.title = 'busStudents';

        activate();

        function activate() { }
    }
})();
