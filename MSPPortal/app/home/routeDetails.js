﻿(function () {
    'use strict';

    angular
        .module('home')
        .controller('routeDetails', routeDetails);

    routeDetails.$inject = ['$scope','$mdDialog']; 

    function routeDetails($scope,$mdDialog) {
        $scope.title = 'routeDetails';

        activate();

        function activate() { }

        $scope.addRoute = function () {
            $mdDialog.show({
                templateUrl: '/app/home/addRoute.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })
           .then(function (res) {

           }, function () {

           });
        };

        $scope.addPickUpPoints = function () {
            $mdDialog.show({
                templateUrl: '/app/home/addStudentBus.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })
           .then(function (res) {

           }, function () {

           });
        };
    }
})();
