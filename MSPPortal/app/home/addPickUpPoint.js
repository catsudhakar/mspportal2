﻿(function () {
    'use strict';

    angular
        .module('home')
        .controller('addPickUpPointCtrl', addPickUpPoint);

    addPickUpPoint.$inject = ['$scope']; 

    function addPickUpPoint($scope) {
        $scope.title = 'addPickUpPoint';

        activate();

        function activate() { }
    }
})();
