﻿(function () {
    'use strict';

    angular
        .module('home')
        .controller('addStudentBusCtrl', addStudentBus);

    addStudentBus.$inject = ['$scope','$mdDialog']; 

    function addStudentBus($scope, $mdDialog) {
        $scope.title = 'addStudentBus';

        activate();

        function activate() { }

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();
