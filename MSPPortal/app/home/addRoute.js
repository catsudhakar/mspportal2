﻿(function () {
    'use strict';

    angular
        .module('home')
        .controller('addRouteCtrl', addRoute);

    addRoute.$inject = ['$scope','$mdDialog']; 

    function addRoute($scope, $mdDialog) {
        $scope.title = 'addRoute';

        activate();

        function activate() { }

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();
