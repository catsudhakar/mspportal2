﻿(function () {
    'use strict';

    angular
        .module('home')
        .controller('busDetails', busDetails);

    busDetails.$inject = ['$scope', '$mdDialog'];

    function busDetails($scope, $mdDialog) {
        $scope.title = 'busDetails';

        activate();

        function activate() {
        }

        $scope.addBus = function () {
            $mdDialog.show({
                templateUrl: '/app/home/addBus.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })
           .then(function (res) {

            }, function () {

            });
        };

        $scope.addBusStudent = function () {
            $mdDialog.show({
                templateUrl: '/app/home/addStudentBus.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })
           .then(function (res) {

           }, function () {

           });
        };

    }
})();
