﻿/// <reference path="home/busDetails.html" />
(function () {
    'use strict';
    var app = angular.module('app');
    app.constant('routes', getRoutes());

    app.config(['$routeProvider', '$locationProvider', 'routes', routeConfigurator]);
    function routeConfigurator($routeProvider, $locationProvider, routes) {
        $routeProvider.when(
            '/login', {
                templateUrl: 'app/user/login.html',
                title: 'login',
                layout: 'basic',
                settings: {
                    nav: 1,
                }, public: true, login: true
            });
        routes.forEach(function (r) {
            $routeProvider.when(r.url, r.config);
        });
        $routeProvider.otherwise({ redirectTo: '/login' });

    }
    // Define the routes 
    function getRoutes() {
        return [
             {
                 url: '/',
                 config: {
                     templateUrl: '/app/user/login.html',
                     title: 'login',
                     settings: {
                         nav: 1,
                     }
                 }
             },
            {
                url: '/dashboard',
                config: {
                    templateUrl: 'app/dashboard/dashboard.html',
                    title: 'dashboard',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-dashboard"></i> Dashboard'
                    }
                }
            }, {
                url: '/productadmin',
                config: {
                    title: 'Product Management',
                    templateUrl: 'app/admin/productadmin.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-life-ring"></i> Product Management'
                    }
                }
            },
            , {
                url: '/accountPayble',
                config: {
                    title: 'accountPayble',
                    templateUrl: 'app/accountPayble/vendors.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-life-ring"></i> AccountPayble'
                    }
                }
            },
            {
                url: '/ordertracking',
                config: {
                    title: 'Order Tracking',
                    templateUrl: 'app/admin/orderTracking.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-life-ring"></i> Order Tracking'
                    }
                }
            },
            {
                url: '/purchase',
                config: {
                    title: 'Purchases',
                    templateUrl: 'app/purchase/purchaseOrderMain.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-life-ring"></i> Purchase'
                    }
                }
            },
            {
                url: '/sales',
                config: {
                    title: 'Sales',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-life-ring"></i> Sales'
                    }
                }
            },
            {
                url: '/invoices',
                config: {
                    title: 'Invoices',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-life-ring"></i>Invoices'
                    }
                }
            },
            {
                url: '/users',
                config: {
                    title: 'Users',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-life-ring"></i> Users'
                    }
                }
            },
            {
                url: '/reports',
                config: {
                    title: 'Reports',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-life-ring"></i> Reports'
                    }
                }
            },
              {
                  url: '/busDetails',
                  config: {
                      title: 'busDetails',
                      templateUrl: 'app/home/busDetails.html',
                     
                  }
              },
               {
                   url: '/customerlist',
                   config: {
                       title: 'customerlist',
                       templateUrl: 'app/user/customerlist.html',
                   }
               },
               {
                   url: '/supplierList',
                   config: {
                       title: 'supplierList',
                       templateUrl: 'app/user/supplierList.html',
                   }
               },
               {
                   url: '/productsList',
                   config: {
                       title: 'productsList',
                       templateUrl: 'app/Products/productList.html',
                   }
               },
               {
                   url: '/categoryList',
                   config: {
                       title: 'categoryList',
                       templateUrl: 'app/Products/categoryList.html',
                   }
               }
               ,
               {
                   url: '/supplierOrderList',
                   config: {
                       title: 'supplierOrderList',
                       templateUrl: 'app/orderTracking/supplierOrderList.html',
                   }
               }
               ,
               {
                   url: '/clientOrderList',
                   config: {
                       title: 'clientOrderList',
                       templateUrl: 'app/orderTracking/clientOrderList.html',
                   }
               },
               {
                   url: '/vendors',
                   config: {
                       title: 'vendors',
                       templateUrl: 'app/accountPayble/vendors.html',
                   }
               },
               {
                   url: '/purchaseOrderMain',
                   config: {
                       title: 'purchaseorders',
                       templateUrl: 'app/accountPayble/purchase/purchaseOrderMain.html',
                   }
               }
               ,
               {
                   url: '/purchaseOrderList',
                   config: {
                       title: 'purchaseOrderList',
                       templateUrl: 'app/accountPayble/purchase/purchaseOrderList.html',
                   }
               },
               {
                   url: '/purchaseOrderLineList',
                   config: {
                       title: 'purchaseOrderList',
                       templateUrl: 'app/accountPayble/purchase/purchaseOrderLineList.html',
                   }
               },
               {
                   url: '/purchaseOrder',
                   config: {
                       title: 'purchaseOrder',
                       templateUrl: 'app/accountPayble/purchase/purchaseOrder.html',
                   }
               },
               {
                   url: '/vendorMain',
                   config: {
                       title: 'vendorMain',
                       templateUrl: 'app/accountPayble/vendor/vendorMain.html',
                   }
               },
               {
                   url: '/vendorInvoiceList',
                   config: {
                       title: 'vendorInvoiceList',
                       templateUrl: 'app/accountPayble/vendorInvoice/vendorInvoiceList.html',
                   }
               },
               {
                   url: '/vendorList',
                   config: {
                       title: 'vendorList',
                       templateUrl: 'app/accountPayble/vendor/vendorList.html',
                   }
               },
               {
                   url: '/vendorAddressList',
                   config: {
                       title: 'vendorAddressList',
                       templateUrl: 'app/accountPayble/vendor/vendorAddressList.html',
                   }
               },
               {
                   url: '/vendorContactList',
                   config: {
                       title: 'vendorContactList',
                       templateUrl: 'app/accountPayble/vendor/vendorContactList.html',
                   }
               },
               {
                   url: '/companyAddress',
                   config: {
                       title: 'companyAddress',
                       templateUrl: 'app/admin/companyAddress.html',
                   }
               },
               {
                   url: '/companyAddressList',
                   config: {
                       title: 'companyAddressList',
                       templateUrl: 'app/companyAddress/companyAddressList.html',
                   }
               },
               {
                   url: '/customers',
                   config: {
                       title: 'customers',
                       templateUrl: 'app/admin/customerMain.html',
                   }
               },
               {
                   url: '/customersList',
                   config: {
                       title: 'customersList',
                       templateUrl: 'app/accountRecievable/customers/customersList.html',
                   }
               },
               {
                   url: '/CustomerMain',
                   config: {
                       title: 'addNewCustomerList',
                       templateUrl: 'app/accountRecievable/customers/CustomerMain.html',
                   }
               },
               {
                   url: '/customerInvoiceMain',
                    config: {
                        title: 'customerInvoiceMain',
                        templateUrl: 'app/accountRecievable/customerInvoice/customerInvoiceMain.html',
                    }
               },
               {
                   url: '/customerInvoiceList',
                   config: {
                       title: 'customerInvoiceList',
                       templateUrl: 'app/accountRecievable/customerInvoice/customerInvoiceList.html',
                   }
               },
               {
                   url: '/salesOrderMain',
                   config: {
                       title: 'salesOrderMain',
                       templateUrl: 'app/accountRecievable/salesOrder/salesOrderMain.html',
                   }
               },
               {
                   url: '/salesOrderLineList',
                   config: {
                       title: 'salesOrderLineList',
                       templateUrl: 'app/accountRecievable/salesOrder/salesOrderLineList.html',
                   }
               },
               {
                   url: '/salesOrderList',
                   config: {
                       title: 'salesOrderList',
                       templateUrl: 'app/accountRecievable/salesOrder/salesOrderList.html',
                   }
               },
               {
                   url: '/salesInvoice',
                   config: {
                       title: 'salesInvoice',
                       templateUrl: 'app/accountRecievable/salesOrder/salesInvoice.html',
                   }
               },
               {
                   url: '/currencyList',
                   config: {
                       title: 'currencyList',
                       templateUrl: 'app/currency/currencyList.html',
                   }
               },
               {
                   url: '/siteList',
                   config: {
                       title: 'siteList',
                       templateUrl: 'app/sites/siteList.html',
                   }
               },
               {
                   url: '/wareHouseList',
                   config: {
                       title: 'wareHouseList',
                       templateUrl: 'app/wareHouse/wareHouseList.html',
                   }
               },
               {
                   url: '/vendorInvoiceMain',
                   config: {
                       title: 'vendorInvoiceMain',
                       templateUrl: 'app/accountPayble/vendorInvoice/vendorInvoiceMain.html',
                   }
               }
        ];
    }
})();


